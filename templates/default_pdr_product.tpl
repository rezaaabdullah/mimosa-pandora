<?php
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
{
	if (isset($_POST['token']) && isset($_SESSION['token']) && $_POST['token'] == $_SESSION['token'])
	{
    if (isset($_POST['paginate_table']))
    {
			$display->table_product($get_product['product'], $_POST['item_per_page']);
    }

		if (isset($_POST['change_product_status']))
    {
			$display->table_product($get_product['product'], $_POST['item_per_page'], $request);
    ?>

		<script>
			$(document).ready(function() {
				$('.pagination-wrapper').append('<div class="pagination"></div>');
			});
		</script>

		<?php
			$display->pagination(array(
				'total_data' => count($get_product['product']),
				'item_per_page' => $_POST['item_per_page'],
			));
    }

    if (isset($_POST['change_pagination']))
    {
			$display->table_product($get_product['product'], $_POST['item_per_page']);
    ?>

		<script>
			$(document).ready(function() {
				$('.pagination-wrapper').append('<div class="pagination"></div>');
			});
		</script>

		<?php
			$display->pagination(array(
				'total_data' => count($get_product['product']),
				'item_per_page' => $_POST['item_per_page'],
			));
    }
  }
}
else
{
?>
<div class="main-content container-fluid">
  <div class="col-xs-12 col-sm-5">
    <h1 class="title">Product Data</h1>
    <p class="desc">
      Lorem ipsum dolor sit amet, consectetur adipisicing elit,
      sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
    </p>
  </div>
	<div class="col-sm-12">
		<div class="table-option">
			<div class="left col-xs-3">
				Product status
				<select class="product-status" onchange="change_product_status()">
					<?php
					for ($i=0; $i < count($product_status); $i++)
					{
						if (isset($_SESSION['product_status']) && $product_status[$i] == $_SESSION['product_status'])
						{
						?>
						<option selected><?php echo $product_status[$i]; ?></option>
						<?php
						}
						else
						{
						?>
						<option><?php echo $product_status[$i]; ?></option>
						<?php
						}
					}
					?>
				</select>

				<script type="text/javascript">
				function change_product_status()
				{
					var product_status = $('.product-status').val();
					if (product_status != '')
	        {
						$(".table-wrapper").empty();
						$('.table-wrapper').prepend('<div class="loading-indication-mini">\
						<img src="assets/img/loading-mini.gif"/>\
						</div>');
	          $('.pagination').remove();

	          var dataString = {'change_product_status' : 'change_product_status', 'request' : product_status, 'token' : '<?php echo $_SESSION['token'];?>', 'item_per_page' : $('.item_per_page').val()};
	          $.ajax({
	              type: "POST",
	              url: "<?php echo $active_page; ?>",
	              data: dataString,
	              cache: false,
	              success: function(data)
	              {
	                $(".table-wrapper").html(data);
	                $('.loading-indication').remove();
	              },
	              error: function(err)
	              {
	                console.log(err);
	                $('.loading-indication').remove();
	              }
	          });
	        }
				}
	      </script>
			</div>

			<div class="middle col-xs-6">
				<div class="search-wrapper">
					<form id="form-search">
						<input type="text" class="form-control" placeholder="Search here..">
						<button class="btn btn-search"><i class="fas fa-search"></i></button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-12">
	  <div class="table-wrapper table-responsive">
			<?php
			$display->table_product($get_product['product'], $item_per_page);

			$display->pagination(
				array(
					'total_data' => count($get_product['product']),
					'item_per_page' => $item_per_page,
				)
			);
			?>
	  </div>
	</div>

  <div class="col-xs-12 table-control">
    <div class="item-option-wrapper col-xs-12 col-sm-3">
      <label>Item per page</label>
      <select class="item_per_page">
				<?php
				for ($i=0; $i < count($item_per_page_list); $i++)
				{
					if (isset($_SESSION['item_per_page']) && $item_per_page_list[$i] == $_SESSION['item_per_page'])
					{
					?>
	        <option selected><?php echo $item_per_page_list[$i]; ?></option>
					<?php
					}
					else
					{
					?>
	        <option><?php echo $item_per_page_list[$i]; ?></option>
					<?php
					}
				}
				?>
      </select>

      <script type="text/javascript">
      $('.item_per_page').change(function(e)
      {
        if ($(this).val() != '')
        {
					$(".table-wrapper").empty();
					$('.table-wrapper').prepend('<div class="loading-indication-mini">\
					<img src="assets/img/loading-mini.gif"/>\
					</div>');
          $('.pagination').remove();

          var dataString = {'change_pagination' : 'change_pagination', 'token' : '<?php echo $_SESSION['token'];?>', 'item_per_page' : $(this).val(), 'request' : $('.product-status').val()};
          $.ajax({
              type: "POST",
              url: "<?php echo $active_page; ?>",
              data: dataString,
              cache: false,
              success: function(data)
              {
                $(".table-wrapper").html(data);
                $('.loading-indication').remove();
              },
              error: function(err)
              {
                console.log(err);
                $('.loading-indication').remove();
              }
          });
        }
      });
      </script>
    </div>

		<div class="pagination-wrapper col-xs-12 col-sm-9">
    	<div class="pagination"></div>
		</div>
  </div>
	<?php
	if (isset($_SESSION['product_status']))
	{
	?>
	<script type="text/javascript">
	change_product_status();
	</script>
	<?php
	}
	?>
</div>
<?php
}
?>
