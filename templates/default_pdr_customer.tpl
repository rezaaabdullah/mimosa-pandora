<?php
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
{
	if (isset($_POST['token']) && isset($_SESSION['token']) && $_POST['token'] == $_SESSION['token'])
	{
		if (isset($_POST['enable_disable_customer']))
    {
			if (isset($save_customer['errlog']))
			{
				if ($save_customer['errlog'] == '0')
				{
				?>
				<div class="success-notif">
					<i class="fas fa-check"></i>
					<p>Success <?php echo $action; ?> customer</p>
				</div>
				<div class="notif-footer">
					<button class="btn btn-close btn-success" data-toggle="modal" data-dismiss="modal">Done</button>
				</div>
				<script>
				change_user_status();
				</script>
				<?php
				}
				else
				{
				?>
				<div class="failed-notif">
					<i class="fas fa-times"></i>
					<p>Failed to <?php echo $action; ?> customer data</p>
					<p><?php echo $save_customer['errdesc']; ?>
				</div>
				<div class="notif-footer">
					<button class="btn btn-close btn-danger" data-toggle="modal" data-dismiss="modal">Dismiss</button>
				</div>
				<?php
				}
			}
			?>
			<script>
			$('#ModalEnableDisable').modal('hide');

			$('#ModalEnableDisable').on('hidden.bs.modal', function ()
			{
				var modal_notification_status = $('#modal-notification').attr('class');

				if(!modal_notification_status.includes('closed'))
				{
			  	$('#modal-notification').modal('show');
					$('#modal-notification').addClass('closed');
				}
			});
			</script>
			<?php
		}

		if (isset($_POST['get_enable_disable_customer']))
    {
		?>
		<div class="modal-ed-body">
			<i class="fas fa-exclamation"></i>
			<p class="attention">Are you sure want to <?php echo $attention; ?> this customer?</p>

			<div class="button-wrapper">
				<form method="POST" id="form-enable-disable">
					<div class="form-group">
						<input type="hidden" name="cust_sqid" id="cust_sqid_enable_disable" class="form-control" value="<?php echo $_POST['cust_sqid']; ?>" required>
						<input type="hidden" name="cust_status" id="cust_status_enable_disable" class="form-control" value="<?php echo $_POST['user_status']; ?>" required>
					</div>
					<button type="submit" class="btn btn-close btn-danger">
						<?php echo $attention; ?>
					</button>
					<button class="btn btn-close btn-default" data-toggle="modal" data-dismiss="modal">Cancel</button>
				</form>

				<script>
				$('#form-enable-disable').submit(function(e)
				{
					e.preventDefault();
					var dataString = {'enable_disable_customer' : 'enable_disable_customer', 'token' : '<?php echo $_SESSION['token'];?>', 'data' : $(this).serializeArray()};
					$.ajax({
						type: "POST",
						url: "<?php echo $active_page; ?>",
						data: dataString,
						cache: false,
						success: function(data)
						{
							$('#modal-notification').removeClass('closed');
							$(".response-enable-disable").html(data);
						},
						error: function(err)
						{
							console.log(err);
						}
					});
				});
				</script>
			</div>
		</div>
		<?php
		}

		if (isset($_POST['edit_customer']))
    {
			if (isset($save_customer['errlog']))
			{
				if ($save_customer['errlog'] == '0')
				{
				?>
				<div class="success-notif">
					<i class="fas fa-check"></i>
					<p>Success edit customer data</p>
				</div>
				<div class="notif-footer">
					<button class="btn btn-close btn-success" data-toggle="modal" data-dismiss="modal">Done</button>
				</div>
				<script>
				change_user_status();
				</script>
				<?php
				}
				else
				{
				?>
				<div class="failed-notif">
					<i class="fas fa-times"></i>
					<p>Failed to edit customer data</p>
					<p><?php echo $save_customer['errdesc']; ?>
				</div>
				<div class="notif-footer">
					<button class="btn btn-close btn-danger" data-toggle="modal" data-dismiss="modal">Dismiss</button>
				</div>
				<?php
				}
			}
			?>
			<script>
			$('#ModalEdit').modal('hide');

			$('#ModalEdit').on('hidden.bs.modal', function ()
			{
				var modal_notification_status = $('#modal-notification').attr('class');

				if(!modal_notification_status.includes('closed'))
				{
			  	$('#modal-notification').modal('show');
					$('#modal-notification').addClass('closed');
				}
			});
			</script>
			<?php
		}

		if (isset($_POST['get_edit_customer']))
    {
			if (isset($customer))
			{
			?>
			<form method="POST" id="form-edit">
				<div class="form-group">
					<label>Company Name:</label>
					<input type="text" name="cust_company" id="cust_company_edit" class="form-control" value="<?php echo $customer['cust_company']; ?>" required>
				</div>
				<div class="form-group">
					<label>Full Name:</label>
					<input type="text" name="cust_fullname" id="cust_fullname_edit" class="form-control" value="<?php echo $customer['cust_fullname']; ?>" required>
				</div>
				<div class="form-group">
					<label>Phone Number:</label>
					<input type="text" name="cust_phone" id="cust_phone_edit" class="form-control" value="<?php echo $customer['cust_phone']; ?>" required>
				</div>
				<div class="form-group">
					<label>Address:</label>
					<input type="text" name="cust_address" id="cust_address_edit" class="form-control" value="<?php echo $customer['cust_address']; ?>" required>
				</div>
				<!-- <div class="form-group">
					<label>City:</label>
					<select name="cust_city" id="cust_city_edit" class="form-control" required>
						<option><?php echo $customer['cust_city']; ?></option>
					</select>
				</div>
				<div class="form-group">
					<label>Province:</label>
					<select name="cust_province" id="cust_province_edit" class="form-control" required>
						<option><?php echo $customer['cust_province']; ?></option>
					</select>
				</div> -->
				<div class="form-group">
					<label>Country:</label>
					<select name="cust_country" id="cust_country_edit" class="form-control" required>
						<option value="ID"><?php echo $customer['cust_country']; ?></option>
					</select>
				</div>
				<div class="form-group">
					<label>ZIP Code:</label>
					<input type="text" name="cust_zipcode" id="cust_zipcode_edit" class="form-control" value="<?php echo $customer['cust_zipcode']; ?>" required>
				</div>
				<div class="form-group" style="display: none;">
					<input type="hidden" name="cust_sqid" id="cust_sqid_edit" class="form-control" value="<?php echo $customer['cust_sqid']; ?>" required>
				</div>
				<div class="form-group">
					<input type="hidden" name="submit-add-black-domain" style="opacity: 0; display: none;" value="true">
					<button type="submit" class="btn btn-warning" style="width: 100%">
						Save
					</button>
				</div>
			</form>

			<script>
			$('#form-edit').submit(function(e)
			{
				e.preventDefault();
				var dataString = {'edit_customer' : 'edit_customer', 'token' : '<?php echo $_SESSION['token'];?>', 'data' : $(this).serializeArray()};
				$.ajax({
					type: "POST",
					url: "<?php echo $active_page; ?>",
					data: dataString,
					cache: false,
					success: function(data)
					{
						$('#modal-notification').removeClass('closed');
						$(".response-edit").html(data);
					},
					error: function(err)
					{
						console.log(err);
					}
				});
			});
			</script>
			<?php
			}
		}

		if (isset($_POST['add_customer']))
    {
			if (isset($save_customer['errlog']))
			{
				if ($save_customer['errlog'] == '0')
				{
				?>
				<div class="success-notif">
					<i class="fas fa-check"></i>
					<p>Success add new customer</p>
				</div>
				<div class="notif-footer">
					<button class="btn btn-close btn-success" data-toggle="modal" data-dismiss="modal">Done</button>
				</div>
				<script>
				change_user_status();
				</script>
				<?php
				}
				else
				{
				?>
				<div class="failed-notif">
					<i class="fas fa-times"></i>
					<p>Failed to add new customer</p>
					<p><?php echo $save_customer['errdesc']; ?>
				</div>
				<div class="notif-footer">
					<button class="btn btn-close btn-danger" data-toggle="modal" data-dismiss="modal">Dismiss</button>
				</div>
				<?php
				}
			}
			?>
			<script>
			$('#ModalAdd').modal('hide');

			$('#ModalAdd').on('hidden.bs.modal', function ()
			{
				var modal_notification_status = $('#modal-notification').attr('class');

				if(!modal_notification_status.includes('closed'))
				{
			  	$('#modal-notification').modal('show');
					$('#modal-notification').addClass('closed');
				}
			});
			</script>
			<?php
		}

    if (isset($_POST['paginate_table']))
    {
			$display->table_customer($get_customer['customer'], $_POST['item_per_page'], $request);
    }

		if (isset($_POST['change_user_status']))
    {
			$display->table_customer($get_customer['customer'], $_POST['item_per_page'], $request);
    ?>

		<script>
			$(document).ready(function() {
				$('.pagination-wrapper').append('<div class="pagination"></div>');
			});
		</script>

		<?php
			$display->pagination(array(
				'total_data' => count($get_customer['customer']),
				'item_per_page' => $_POST['item_per_page'],
			));
    }

    if (isset($_POST['change_pagination']))
    {
			$display->table_customer($get_customer['customer'], $_POST['item_per_page'], $request);
    ?>

		<script>
			$(document).ready(function() {
				$('.pagination-wrapper').append('<div class="pagination"></div>');
			});
		</script>

		<?php
			$display->pagination(array(
				'total_data' => count($get_customer['customer']),
				'item_per_page' => $_POST['item_per_page'],
			));
    }
  }
}
else
{
?>
<div class="main-content container-fluid">
  <div class="col-xs-12 col-sm-5">
    <h1 class="title">Customer Data</h1>
    <p class="desc">
      Lorem ipsum dolor sit amet, consectetur adipisicing elit,
      sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
    </p>
  </div>
	<div class="col-sm-12">
		<div class="table-option">
			<div class="left col-xs-3">
				User status
				<select class="user-status" onchange="change_user_status()">
					<?php
					for ($i=0; $i < count($user_status); $i++)
					{
						if (isset($_SESSION['user_status']) && $user_status[$i] == $_SESSION['user_status'])
						{
						?>
						<option selected><?php echo $user_status[$i]; ?></option>
						<?php
						}
						else
						{
						?>
						<option><?php echo $user_status[$i]; ?></option>
						<?php
						}
					}
					?>
				</select>

				<script type="text/javascript">
				function change_user_status()
				{
					var user_status = $('.user-status').val();
					if (user_status != '')
	        {
						$(".table-wrapper").empty();
						$('.table-wrapper').prepend('<div class="loading-indication-mini">\
						<img src="assets/img/loading-mini.gif"/>\
						</div>');
	          $('.pagination').remove();

	          var dataString = {'change_user_status' : 'change_user_status', 'request' : user_status, 'token' : '<?php echo $_SESSION['token'];?>', 'item_per_page' : $('.item_per_page').val()};
	          $.ajax({
	              type: "POST",
	              url: "<?php echo $active_page; ?>",
	              data: dataString,
	              cache: false,
	              success: function(data)
	              {
	                $(".table-wrapper").html(data);
	                $('.loading-indication').remove();
	              },
	              error: function(err)
	              {
	                console.log(err);
	                $('.loading-indication').remove();
	              }
	          });
	        }
				}
	      </script>
			</div>

			<div class="middle col-xs-6">
				<div class="search-wrapper">
					<form id="form-search">
						<input type="text" class="form-control" placeholder="Search here.." required>
						<button class="btn btn-search"><i class="fas fa-search"></i></button>
					</form>
				</div>
			</div>

			<div class="right col-xs-3">
				<button class="btn btn-defaut" data-toggle="modal" data-target="#ModalAdd">
					<i class="fas fa-user-plus"></i> Add Customer
				</button>
			</div>
		</div>
	</div>

	<div class="col-sm-12">
	  <div class="table-wrapper table-responsive">
			<?php
			$display->table_customer($get_customer['customer'], $item_per_page);

			$display->pagination(
				array(
					'total_data' => count($get_customer['customer']),
					'item_per_page' => $item_per_page,
				)
			);
			?>
	  </div>
	</div>

  <div class="col-xs-12 table-control">
    <div class="item-option-wrapper col-xs-12 col-sm-3">
      <label>Item per page</label>
      <select class="item_per_page">
				<?php
				for ($i=0; $i < count($item_per_page_list); $i++)
				{
					if (isset($_SESSION['item_per_page']) && $item_per_page_list[$i] == $_SESSION['item_per_page'])
					{
					?>
	        <option selected><?php echo $item_per_page_list[$i]; ?></option>
					<?php
					}
					else
					{
					?>
	        <option><?php echo $item_per_page_list[$i]; ?></option>
					<?php
					}
				}
				?>
      </select>

      <script type="text/javascript">
      $('.item_per_page').change(function(e)
      {
        if ($(this).val() != '')
        {
					$(".table-wrapper").empty();
					$('.table-wrapper').prepend('<div class="loading-indication-mini">\
					<img src="assets/img/loading-mini.gif"/>\
					</div>');
          $('.pagination').remove();

          var dataString = {'change_pagination' : 'change_pagination', 'token' : '<?php echo $_SESSION['token'];?>', 'item_per_page' : $(this).val(), 'request' : $('.user-status').val()};
          $.ajax({
              type: "POST",
              url: "<?php echo $active_page; ?>",
              data: dataString,
              cache: false,
              success: function(data)
              {
                $(".table-wrapper").html(data);
                $('.loading-indication').remove();
              },
              error: function(err)
              {
                console.log(err);
                $('.loading-indication').remove();
              }
          });
        }
      });
      </script>
    </div>

		<div class="pagination-wrapper col-xs-12 col-sm-9">
    	<div class="pagination"></div>
		</div>
  </div>

	<?php
	if (isset($_SESSION['user_status']))
	{
	?>
	<script type="text/javascript">
	change_user_status();
	</script>
	<?php
	}
	?>
</div>

<div class="modal fade modal-add" id="ModalAdd" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add new customer</h4>
			</div>

			<div class="modal-body">
				<form method="POST" id="form-add">
					<div class="form-group">
						<label>Company Name:</label>
						<input type="text" name="cust_company" id="cust_company" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Full Name:</label>
						<input type="text" name="cust_fullname" id="cust_fullname" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Email:</label>
						<input type="text" name="cust_email" id="cust_email" class="form-control" required>
						<div class="response-validate-email"></div>
					</div>
					<div class="form-group">
						<label>Password:</label>
						<input type="password" name="cust_passwd" id="cust_passwd" class="form-control" required>
						<div class="response-validate-password"></div>
					</div>
					<div class="form-group">
						<label>Retype Password:</label>
						<input type="password" name="retype_cust_passwd" id="retype_cust_passwd" class="form-control" required>
						<div class="response-validate-password"></div>
					</div>
					<div class="form-group">
						<label>Phone Number:</label>
						<input type="text" name="cust_phone" id="cust_phone" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Address:</label>
						<input type="text" name="cust_address" id="cust_address" class="form-control" required>
					</div>
					<!-- <div class="form-group">
						<label>City:</label>
						<select name="cust_city" id="cust_city" class="form-control" required>
							<option>Bandung</option>
						</select>
					</div>
					<div class="form-group">
						<label>Province:</label>
						<select name="cust_province" id="cust_province" class="form-control" required>
							<option>ID-JB</option>
						</select>
					</div> -->
					<div class="form-group">
						<label>Country:</label>
						<select name="cust_country" id="cust_country" class="form-control" required>
							<option>ID</option>
						</select>
					</div>
					<div class="form-group">
						<label>ZIP Code:</label>
						<input type="text" name="cust_zipcode" id="cust_zipcode" class="form-control" required>
					</div>
					<div class="form-group">
						<input type="hidden" name="submit-add-black-domain" style="opacity: 0; display: none;" value="true">
						<button type="submit" class="btn btn-success" style="width: 100%">
							Save
						</button>
					</div>
				</form>

				<script>
				$("#cust_email").blur(function( event )
				{
					validate_email($('#cust_email').val(), 'cust_email');
				});

				$("#cust_passwd, #retype_cust_passwd").change(function( event )
				{
					validate_password('cust_passwd');
				});

				$('#form-add').submit(function(e)
				{
					e.preventDefault();
					var email_status = validate_email($('#cust_email').val(), 'cust_email');
					var pass_status = validate_password('cust_passwd');

					if (email_status === true && pass_status === true)
					{
						var dataString = {'add_customer' : 'add_customer', 'token' : '<?php echo $_SESSION['token'];?>', 'data' : $(this).serializeArray()};
	          $.ajax({
	              type: "POST",
	              url: "<?php echo $active_page; ?>",
	              data: dataString,
	              cache: false,
	              success: function(data)
	              {
									$('#modal-notification').removeClass('closed');
									$('#form-add')[0].reset();
	                $(".response-add").html(data);
	              },
	              error: function(err)
	              {
	                console.log(err);
	              }
	          });
					}
				});
				</script>
			</div>
		</div>
	</div>
</div>

<div class="modal fade modal-edit" id="ModalEdit" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit customer data</h4>
			</div>

			<div class="modal-body response-get-edit">

			</div>

			<script>
			function edit_customer(id, status)
			{
				var dataString = {'get_edit_customer' : 'get_edit_customer', 'token' : '<?php echo $_SESSION['token'];?>', 'cust_sqid' : id, 'user_status' : status};

				$.ajax({
					type: "POST",
					url: "<?php echo $active_page; ?>",
					data: dataString,
					cache: false,
					success: function(data)
					{
						$(".response-get-edit").html(data);
					},
					error: function(err)
					{
						console.log(err);
					}
				});
			}
		</script>
		</div>
	</div>
</div>

<div class="modal fade modal-enable-disable" id="ModalEnableDisable" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content response-get-enable-disable">
		</div>

		<script>
		function enable_disable_customer(id, status)
		{
			var dataString = {'get_enable_disable_customer' : 'get_enable_disable_customer', 'token' : '<?php echo $_SESSION['token'];?>', 'cust_sqid' : id, 'user_status' : status};

			$.ajax({
				type: "POST",
				url: "<?php echo $active_page; ?>",
				data: dataString,
				cache: false,
				success: function(data)
				{
					$(".response-get-enable-disable").html(data);
				},
				error: function(err)
				{
					console.log(err);
				}
			});
		}
		</script>
	</div>
</div>

<?php
$display->modal_notification();
}
?>
