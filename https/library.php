<?php
class display
{
	function table_product($get_product, $item_per_page)
	{
	?>
	<table class="table table-striped">
		<tr>
			<th>prod_min</th>
			<th>prod_max</th>
			<th>prod_code</th>
			<th>prod_part</th>
			<th>prod_name</th>
			<th>prod_month</th>
			<th>prod_idr</th>
		</tr>
		<?php
		if (isset($get_product) && count($get_product) > 0)
		{
			for ($i=0; $i < count($get_product); $i++)
			{
				if ($i == $item_per_page)
				{
					break;
				}
		?>
			<tr>
				<td>
					<?php echo $get_product[$i]['prod_min'];?>
				</td>
				<td>
					<?php echo $get_product[$i]['prod_max'];?>
				</td>
				<td>
					<?php echo $get_product[$i]['prod_code'];?>
				</td>
				<td>
					<?php echo $get_product[$i]['prod_part'];?>
				</td>
				<td>
					<?php echo $get_product[$i]['prod_name'];?>
				</td>
				<td>
					<?php echo $get_product[$i]['prod_month'];?>
				</td>
				<td>
					Rp. <?php echo number_format($get_product[$i]['prod_idr'],2,',','.'); ?>
				</td>
			</tr>
		<?php
			}
		}
		else
		{
		?>
		<tr>
			<td colspan="7"><center><i>Data tidak tersedia</i></center>
		</tr>
		<?php
		}
		?>
	</table>
	<?php
	}

	function table_customer($get_product, $item_per_page, $user_status = 'all')
	{
	?>
	<table class="table table-striped">
		<tr>
			<th>Company</th>
			<th>Fullname</th>
			<th>Email</th>
			<th>Phone</th>
			<th>Address</th>
			<!-- <th>City</th>
			<th>Province</th> -->
			<th>Country</th>
			<th>Zip Code</th>
			<th>Date Entry</th>
			<th>Action</th>
		</tr>
		<?php
		if (isset($get_product) && count($get_product) > 0)
		{
			for ($i=0; $i < count($get_product); $i++)
			{
				if ($i == $item_per_page)
				{
					break;
				}
		?>
			<tr>
				<td>
					<?php echo $get_product[$i]['cust_company'];?>
				</td>
				<td>
					<?php echo $get_product[$i]['cust_fullname'];?>
				</td>
				<td>
					<?php echo $get_product[$i]['cust_email'];?>
				</td>
				<td>
					<?php echo $get_product[$i]['cust_phone'];?>
				</td>
				<td>
					<?php echo $get_product[$i]['cust_address'];?>
				</td>
				<!-- <td>
					<?php //echo $get_product[$i]['cust_city'];?>
				</td>
				<td>
					<?php //echo $get_product[$i]['cust_province'];?>
				</td> -->
				<td>
					<?php echo $get_product[$i]['cust_country'];?>
				</td>
				<td>
					<?php echo $get_product[$i]['cust_zipcode'];?>
				</td>
				<td>
					<?php echo gmdate('Y-m-d H:i:s', $get_product[$i]['cust_dtentry']); ?>
				</td>
				<td>
					<button class="btn btn-warning" data-toggle="modal" data-target="#ModalEdit" onclick="edit_customer('<?php echo $get_product[$i]['cust_sqid'];?>', '<?php echo $get_product[$i]['cust_status'];?>')">
						<i class="fas fa-edit"></i> Edit
					</button>
					<?php
					if ($user_status == 'inactive')
					{
					?>
					<button class="btn btn-success" data-toggle="modal" data-target="#ModalEnableDisable" onclick="enable_disable_customer('<?php echo $get_product[$i]['cust_sqid'];?>', 'active')">
						<i class="far fa-check-circle"></i> Enable
					</button>
					<?php
					}
					else if ($user_status == 'active')
					{
					?>
					<button class="btn btn-danger" data-toggle="modal" data-target="#ModalEnableDisable" onclick="enable_disable_customer('<?php echo $get_product[$i]['cust_sqid'];?>', 'inactive')">
						<i class="fas fa-trash-alt"></i> Disable
					</button>
					<?php
					}
					else if ($user_status == 'all')
					{
						if ($get_product[$i]['cust_status'] == 'active')
						{
						?>
						<button class="btn btn-danger" data-toggle="modal" data-target="#ModalEnableDisable" onclick="enable_disable_customer('<?php echo $get_product[$i]['cust_sqid'];?>', 'inactive')">
							<i class="fas fa-trash-alt"></i> Disable
						</button>
						<?php
						}
						else if ($get_product[$i]['cust_status'] == 'inactive')
						{
						?>
						<button class="btn btn-success" data-toggle="modal" data-target="#ModalEnableDisable" onclick="enable_disable_customer('<?php echo $get_product[$i]['cust_sqid'];?>', 'active')">
							<i class="far fa-check-circle"></i> Enable
						</button>
						<?php
						}
					}
					?>
				</td>
			</tr>
		<?php
			}
		}
		else
		{
		?>
		<tr>
			<td colspan="11"><center><i>Data tidak tersedia</i></center>
		</tr>
		<?php
		}
		?>
	</table>
	<script type="text/javascript">
		$(".select-all").click(function () {
			$('input:checkbox').prop('checked', this.checked);
		});

		$("input:checkbox").not('.select-all').click(function ()
		{
			if ($('input:checkbox:checked').not('.select-all').length == $('input:checkbox').not('.select-all').length)
			{
				$('.select-all').prop('checked', true);
			}
			else
			{
				$('.select-all').prop('checked', false);
			}
		});
	</script>
	<?php
	}

	function pagination($data = array(), $data_status = null)
	{
		global $active_page;
	?>
	<script type="text/javascript">
	$(document).ready(function() {
		$(".pagination").bootpag({
			total: <?php echo ceil($data['total_data']/$data['item_per_page']); ?>,
			page: 1,
			maxVisible: 5,
			leaps: false,
			firstLastUse: true,
			first: 'First',
			last: 'Last',
			prev: 'Prev',
			next: 'Next',
			wrapClass: 'pagination',
			activeClass: 'active',
			disabledClass: 'disabled',
			nextClass: 'next',
			prevClass: 'prev',
			lastClass: 'last',
			firstClass: 'first'
		}).on("page", function(e, num)
		{
			e.preventDefault();

			$('.table-wrapper').empty();
			$('.table-wrapper').prepend('<div class="loading-indication-mini">\
			<img src="assets/img/loading-mini.gif"/>\
			</div>');

			if ($('.user-status').val())
			{
				var dataString = {'paginate_table' : 'paginate_table', 'page' : num, 'token' : '<?php echo $_SESSION['token'];?>', 'item_per_page' : '<?php echo $data['item_per_page'];?>', 'request' : $('.user-status').val()};
			}
			else if ($('.product-status').val())
			{
				var dataString = {'paginate_table' : 'paginate_table', 'page' : num, 'token' : '<?php echo $_SESSION['token'];?>', 'item_per_page' : '<?php echo $data['item_per_page'];?>', 'request' : $('.product-status').val()};
			}
			else
			{
				var dataString = {'paginate_table' : 'paginate_table', 'page' : num, 'token' : '<?php echo $_SESSION['token'];?>', 'item_per_page' : '<?php echo $data['item_per_page'];?>'};
			}
			$.ajax({
					type: "POST",
					url: "<?php echo $active_page; ?>",
					data: dataString,
					cache: false,
					success: function(data)
					{
						$(".table-wrapper").html(data);
						$('.loading-indication-mini').remove();
					},
					error: function(err)
					{
						console.log(err);
						$('.loading-indication-mini').remove();
					}
			});
		});

	});
	</script>
	<?php
	}

	function modal_notification()
	{
	?>
	<div class="modal fade modal-notification" id="modal-notification" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content response-add response-edit response-enable-disable">

			</div>
		</div>
	</div>
	<?php
	}
}

$display = new display();

function templatefile($current_url)
{
	global $dir_tpl;
	global $template_prefix;
	$break = Explode('/', $current_url);
	$pfile = $break[count($break) - 1];
	$file = Explode('.php', $pfile);
	$pname = $file[0];

	if($pfile == '')
	{
		return($dir_tpl.$template_prefix .'_'. $pname.'index.tpl');
	}

	return($dir_tpl.$template_prefix .'_'. $pname.'.tpl');
}

function get_active_page($current_url)
{
	$break = Explode('/', $current_url);
	$pfile = $break[count($break) - 1];

	if($pfile == '')
	{
		return('index.php');
	}

	return($pfile);
}

function url_jump($url, $alert = null)
{
?>
	<script language="javascript">
		<?php
		if ($alert != null)
		{
		?>
		alert('<?php echo $alert ?>');
		<?php
		}
		?>

		window.location="<?php echo $url; ?>";
	</script>
<?php
	exit();
}
?>
