<?php
include_once 'header.php';

$product_status = array('All', 'New', 'Renew');

if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
{
	if (isset($_POST['token']) && isset($_SESSION['token']) && $_POST['token'] == $_SESSION['token'])
	{
    if (isset($_POST['paginate_table']))
    {
			$request = strtolower($_POST['request']);
      if (isset($_POST['page']))
			{
				$page_number = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);

				if(!is_numeric($page_number))
				{
					die('Invalid page number!');
				}
			}

      $headers    = array(
                    "Content-type: application/json",
                    "Accept: application/json",
                    "Authorization: ". apiheadernew_auth($request)
                    );
      $jsonparam  = json_encode(array('request' => $request));
      $req_json   = myCURL($url_apimimosa_product_get, $headers, $jsonparam);
      $get_product   = json_decode($req_json, true);

      $total_all_product = count($get_product['product']);
      $get_product['product'] = array_slice($get_product['product'], (($page_number-1) * $_POST['item_per_page']), $_POST['item_per_page']);
    }

		if (isset($_POST['change_product_status']))
    {
      $_SESSION['product_status'] = $_POST['request'];
      $request = strtolower($_POST['request']);
      $headers    = array(
                    "Content-type: application/json",
                    "Accept: application/json",
                    "Authorization: ". apiheadernew_auth($request)
                    );
      $jsonparam  = json_encode(array('request' => $request));
      $req_json   = myCURL($url_apimimosa_product_get, $headers, $jsonparam);
      $get_product   = json_decode($req_json, true);
    }

    if (isset($_POST['change_pagination']))
    {
			$_SESSION['item_per_page'] = $_POST['item_per_page'];
			$request = strtolower($_POST['request']);

      $headers    = array(
                    "Content-type: application/json",
                    "Accept: application/json",
                    "Authorization: ". apiheadernew_auth($request)
                    );
      $jsonparam  = json_encode(array('request' => $request));
      $req_json   = myCURL($url_apimimosa_product_get, $headers, $jsonparam);
      $get_product   = json_decode($req_json, true);
    }
  }
  else
	{
		url_jump($active_page);
	}
}
else
{
	$_SESSION['token'] = md5(mt_rand(100000,999999));
	$request = (isset($_SESSION['product_status'])) ? strtolower($_SESSION['product_status']) : strtolower($product_status[0]);
	$item_per_page = (isset($_SESSION['item_per_page'])) ? $_SESSION['item_per_page'] : $item_per_page_list[0];

	$headers    = array(
				"Content-type: application/json",
				"Accept: application/json",
				"Authorization: ". apiheadernew_auth($request)
				);
	$jsonparam  = json_encode(array('request' => $request));
	$req_json   = myCURL($url_apimimosa_product_get, $headers, $jsonparam);
	$get_product   = json_decode($req_json, true);
}

include_once 'footer.php';
?>
