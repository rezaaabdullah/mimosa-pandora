<?php
$dir_https = __DIR__;
$dir_lib   = __DIR__."/../lib/";
$dir_tpl   = __DIR__."/../templates/";
$dir_temp  = __DIR__."/../temp/";

/*URL API*/
$base_url_api = 'https://order.prosperita.co.id/api/devel/mimosapandora/';
$url_apimimosa_product_get         = $base_url_api.'apimimosa_product_get.php';
$url_apimimosa_customer_get        = $base_url_api.'apimimosa_customer_get.php';
$url_apimimosa_customer_add        = $base_url_api.'apimimosa_customer_add.php';
$url_apimimosa_customer_edit       = $base_url_api.'apimimosa_customer_edit.php';
$url_apimimosa_customer_editstatus = $base_url_api.'apimimosa_customer_editstatus.php';

$template_prefix = 'default';

$item_per_page_list = array('10', '25', '50', '100', '150', '200');
?>
