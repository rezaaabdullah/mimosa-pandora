function validate_email(email, id)
{
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  if (re.test(email))
  {
    $('#' + id).css('border-color', '#ccc');
    $('#alert-email').remove();
    return true;
  }
  else
  {
    $('#alert-email').remove();
    $('#' + id).css('border-color', '#d43f3a');
    $('.response-validate-email').append('<span class="label label-danger" id="alert-email">Email tidak valid</span>');
    return false;
  }
}

function validate_password(id)
{
  $('.response-validate-password').empty();

  if ($('#' + id).val() != '' && $('#retype_' + id).val() != '')
  {
    if ($('#' + id).val() != $('#retype_' + id).val())
    {
      $('#'+ id +', #retype_'+ id).css('border-color', '#d43f3a');
      $('.response-validate-password').append('<span class="label label-danger" id="alert-password">Password not match</span>');
      return false;
    }
    else
    {
      $('#'+ id +', #retype_'+ id).css('border-color', '#ccc');
      $('.response-validate-password').empty();
      return true;
    }
  }
  return false;
}
