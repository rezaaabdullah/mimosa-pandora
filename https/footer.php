<?php
include templatefile($_SERVER['REQUEST_URI']);

if ((isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') == false)
{
?>

  <div class="footer">
    .:: MIMOSA PANDORA MANAGEMENT - INTERNAL SYSTEM - Prosperita Mitra Indonesia, PT ::.  &copy 2018-<?php echo date('Y'); ?>
  </div>
</body>
</html>
<?php
}
?>

<script type="text/javascript">
$(document).ready(function()
{
  $('[data-toggle="tooltip"]').tooltip(
  {
    container:'body', trigger: 'hover'
  });
});
</script>
