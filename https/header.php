<?php
session_start();

include_once 'config.php';
include_once 'hds_lib/hds_lib.php';
include_once 'hds_lib/hds_setting.php';
include_once 'library.php';

$active_page = get_active_page($_SERVER['REQUEST_URI']);

if ((isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') == false)
{
?>

<!DOCTYPE html>
<html>
<head>
  <link rel="shortcut icon" href="assets/img/icon.ico" />
  <title>.:: MIMOSA PANDORA MANAGEMENT - INTERNAL SYSTEM - Prosperita Mitra Indonesia, PT ::. </title>

  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Content-Type" content="text/html" />

  <script src="assets/jquery-3.3.1.js"></script>

  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css">
  <script src="assets/bootstrap/js/bootstrap.js"></script>

  <link rel="stylesheet" href="assets/fontawesome/css/fontawesome-all.min.css">
  <script>
  window.FontAwesomeConfig = {
    searchPseudoElements: true
  }
  </script>

  <script type="text/javascript" src="assets/bootpag/jquery.bootpag.min.js"></script>

  <link rel="stylesheet" type="text/css" href="assets/css/main.css?v<?php echo filemtime('assets/css/main.css'); ?>">
  <link rel="stylesheet" type="text/css" href="assets/css/navbar.css?v<?php echo filemtime('assets/css/navbar.css'); ?>">
  <script type="text/javascript" src="assets/js/library.js?v<?php echo filemtime('assets/js/library.js'); ?>"></script>
</head>
<body>
  <nav class="navbar navbar-inverse">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a href="index.php">
          <img src="assets/img/icon.png" class="logo">
        </a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav nav-tabs navbar-right">
    	    <li>
            <a href="https://order.prosperita.co.id/idx_home.php" class="btn">Home</a>
          </li>
    	    <li class="btn-dropdown">
              <a href="#" class="dropbtn">Master <i class="fas fa-angle-down"></i></a>
              <div class="dropdown-content">
                <a href="pdr_product.php">Product</a>
                <a href="pdr_customer.php">Customer</a>
              </div>
          </li>
          <li class="btn-dropdown">
              <a href="#" class="dropbtn">
                Reza Abdulah
                <i class="fas fa-angle-down"></i>
              </a>
              <div class="dropdown-content">
                <a href="https://order.prosperita.co.id/index.php?go=logout">Logout</a>
              </div>
          </li>
    	  </ul>
      </div>
    </div>
  </nav>
<?php
}
?>
