<?php
include_once 'header.php';

$user_status = array('All', 'Active', 'Inactive');

if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
{
	if (isset($_POST['token']) && isset($_SESSION['token']) && $_POST['token'] == $_SESSION['token'])
	{
		if (isset($_POST['enable_disable_customer']))
		{
			if ($_POST['data'][1]['value'] == 'inactive')
			{
				$action = 'disable';
				$cust_status = 'inactive';
			}
			else
			{
				$action = 'enable';
				$cust_status = 'active';
			}

			$cust_sqid = $_POST['data'][0]['value'];

			$headers    = array(
				"Content-type: application/json",
				"Accept: application/json",
				"Authorization: ". apiheadernew_auth($cust_status.$cust_sqid)
			);
			$jsonparam  = json_encode(array('cust_status' => $cust_status, 'cust_sqid' => $cust_sqid));
			$req_json   = myCURL($url_apimimosa_customer_editstatus, $headers, $jsonparam);
			$save_customer   = json_decode($req_json, true);
		}

		if (isset($_POST['get_enable_disable_customer']))
    {
			if ($_POST['user_status'] == 'inactive')
			{
				$modal_title = 'Enable Customer';
				$attention = 'disable';
			}
			else
			{
				$modal_title = 'Disable Customer';
				$attention = 'enable';
			}
		}

		if (isset($_POST['edit_customer']))
    {
      $cust_company   = $_POST['data'][0]['value'];
      $cust_fullname  = $_POST['data'][1]['value'];
      $cust_phone     = $_POST['data'][2]['value'];
      $cust_address   = $_POST['data'][3]['value'];
      $cust_city      = '';//$_POST['data'][4]['value'];
      $cust_province  = '';//$_POST['data'][5]['value'];
      $cust_country   = $_POST['data'][4]['value'];
      $cust_zipcode   = $_POST['data'][5]['value'];
			$cust_sqid	    = $_POST['data'][6]['value'];

      $headers    = array(
                    "Content-type: application/json",
                    "Accept: application/json",
                    "Authorization: ". apiheadernew_auth($cust_company.$cust_sqid)
                    );
      $jsonparam  = json_encode(array('cust_company' => $cust_company, 'cust_fullname' => $cust_fullname, 'cust_phone' => $cust_phone, 'cust_address' => $cust_address, 'cust_city' => $cust_city, 'cust_province' => $cust_province, 'cust_country' => $cust_country, 'cust_zipcode' => $cust_zipcode, 'cust_sqid' => $cust_sqid));
      $req_json   = myCURL($url_apimimosa_customer_edit, $headers, $jsonparam);
      $save_customer   = json_decode($req_json, true);
    }

		if (isset($_POST['get_edit_customer']))
    {
			$request = strtolower($_POST['user_status']);
      $headers    = array(
                    "Content-type: application/json",
                    "Accept: application/json",
                    "Authorization: ". apiheadernew_auth($request)
                    );
      $jsonparam  = json_encode(array('request' => $request));
      $req_json   = myCURL($url_apimimosa_customer_get, $headers, $jsonparam);
      $get_customer   = json_decode($req_json, true);

			if (isset($get_customer['customer']))
			{
				for ($i=0; $i < count($get_customer['customer']); $i++)
				{
					if ($get_customer['customer'][$i]['cust_sqid'] == $_POST['cust_sqid'])
					{
						$customer = $get_customer['customer'][$i];
					}
				}
			}
		}

    if (isset($_POST['add_customer']))
    {
      $cust_company   = $_POST['data'][0]['value'];
      $cust_fullname  = $_POST['data'][1]['value'];
      $cust_email     = $_POST['data'][2]['value'];
      $cust_passwd    = $_POST['data'][3]['value'];
      $cust_phone     = $_POST['data'][5]['value'];
      $cust_address   = $_POST['data'][6]['value'];
      $cust_city      = '';//$_POST['data'][7]['value'];
      $cust_province  = '';//$_POST['data'][8]['value'];
      $cust_country   = $_POST['data'][7]['value'];
      $cust_zipcode   = $_POST['data'][8]['value'];

      $headers    = array(
                    "Content-type: application/json",
                    "Accept: application/json",
                    "Authorization: ". apiheadernew_auth($cust_company.$cust_email)
                    );
      $jsonparam  = json_encode(array('cust_company' => $cust_company, 'cust_fullname' => $cust_fullname, 'cust_email' => $cust_email, 'cust_passwd' => $cust_passwd, 'cust_phone' => $cust_phone, 'cust_address' => $cust_address, 'cust_city' => $cust_city, 'cust_province' => $cust_province, 'cust_country' => $cust_country, 'cust_zipcode' => $cust_zipcode));
      $req_json   = myCURL($url_apimimosa_customer_add, $headers, $jsonparam);
      $save_customer   = json_decode($req_json, true);
    }

    if (isset($_POST['paginate_table']))
    {
      $request = strtolower($_POST['request']);
      if (isset($_POST['page']))
			{
				$page_number = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);

				if(!is_numeric($page_number))
				{
					die('Invalid page number!');
				}
			}

      $headers    = array(
                    "Content-type: application/json",
                    "Accept: application/json",
                    "Authorization: ". apiheadernew_auth($request)
                    );
      $jsonparam  = json_encode(array('request' => $request));
      $req_json   = myCURL($url_apimimosa_customer_get, $headers, $jsonparam);
      $get_customer   = json_decode($req_json, true);

      $total_all_product = count($get_customer['customer']);
      $get_customer['customer'] = array_slice($get_customer['customer'], (($page_number-1) * $_POST['item_per_page']), $_POST['item_per_page']);
    }

    if (isset($_POST['change_user_status']))
    {
      $_SESSION['user_status'] = $_POST['request'];
      $request = strtolower($_POST['request']);
      $headers    = array(
                    "Content-type: application/json",
                    "Accept: application/json",
                    "Authorization: ". apiheadernew_auth($request)
                    );
      $jsonparam  = json_encode(array('request' => $request));
      $req_json   = myCURL($url_apimimosa_customer_get, $headers, $jsonparam);
      $get_customer   = json_decode($req_json, true);
    }

    if (isset($_POST['change_pagination']))
    {
			$_SESSION['item_per_page'] = $_POST['item_per_page'];
      $request = strtolower($_POST['request']);
      $headers    = array(
                    "Content-type: application/json",
                    "Accept: application/json",
                    "Authorization: ". apiheadernew_auth($request)
                    );
      $jsonparam  = json_encode(array('request' => $request));
      $req_json   = myCURL($url_apimimosa_customer_get, $headers, $jsonparam);
      $get_customer   = json_decode($req_json, true);
    }
  }
  else
	{
		url_jump($active_page);
	}
}
else
{
	$_SESSION['token'] = md5(mt_rand(100000,999999));
  $request = (isset($_SESSION['user_status'])) ? strtolower($_SESSION['user_status']) : strtolower($user_status[0]);
	$item_per_page = (isset($_SESSION['item_per_page'])) ? $_SESSION['item_per_page'] : $item_per_page_list[0];

	$headers    = array(
				"Content-type: application/json",
				"Accept: application/json",
				"Authorization: ". apiheadernew_auth($request)
				);
	$jsonparam  = json_encode(array('request' => $request));
	$req_json   = myCURL($url_apimimosa_customer_get, $headers, $jsonparam);
	$get_customer   = json_decode($req_json, true);
}

include_once 'footer.php';
?>
